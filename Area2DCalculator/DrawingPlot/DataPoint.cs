﻿using System;
using System.Drawing;

namespace Area2DCalculator.DrawingPlot
{
    public class DataPoint
    {
        public Point ScreenPoint { get; private set; }
        public PointF ValuePoint { get; }
        public Rectangle PointRect { get; private set; }
        public string Label { get; set; }

        public DataPoint(Point screenPnt, PointF valuePnt, 
                         Rectangle pointRect, string label = "")
        {
            ScreenPoint = screenPnt;
            ValuePoint = valuePnt;
            PointRect = pointRect;
            Label = label;
        }

        public DataPoint(Point screenPnt, PointF valuePnt,
                         int pointSize, string label = "")
        {
            ScreenPoint = screenPnt;
            ValuePoint = valuePnt;
            PointRect = new Rectangle(screenPnt.X - pointSize / 2, 
                                      screenPnt.Y - pointSize / 2, 
                                      pointSize, pointSize);
            Label = label;
        }

        /// <summary>
        /// Apply translation rule using new scaling factors
        /// </summary>
        /// <param name="newXScaleFactor"></param>
        /// <param name="newYScaleFactor"></param>
        /// <param name="axisOffset"></param>
        /// <param name="workRect"></param>
        /// <param name="pointSize"></param>
        /// <param name="minX"></param>
        /// <param name="minY"></param>
        public void Translate(double newXScaleFactor, double newYScaleFactor, int axisOffset,
                              Rectangle workRect, int pointSize, double minX, double minY)
        {
            ScreenPoint = ScreenPointFromUserCoords(ValuePoint, axisOffset, newXScaleFactor,
                                                    newYScaleFactor, minX, minY, workRect);
            PointRect = new Rectangle(ScreenPoint.X - pointSize / 2,
                                      ScreenPoint.Y - pointSize / 2,
                                      pointSize, pointSize);
        }

        /// <summary>
        /// Calculate Point value in user-provided data coordinates
        /// </summary>
        /// <param name="screenPoint"></param>
        /// <param name="axisOffset"></param>
        /// <param name="xScaleFactor"></param>
        /// <param name="yScaleFactor"></param>
        /// <param name="minX"></param>
        /// <param name="maxY"></param>
        /// <returns></returns>
        public static PointF PointFromScreenCoords(Point screenPoint, int axisOffset, 
                                                  double xScaleFactor, double yScaleFactor,
                                                  double minX, double maxY)
        {
            Point pureScreenPoint = new Point(screenPoint.X - axisOffset, screenPoint.Y);
            return new PointF((float)(minX + xScaleFactor * pureScreenPoint.X), 
                             (float)(maxY - yScaleFactor * pureScreenPoint.Y));
        }

        /// <summary>
        /// Create screen Point based on user-supplied coordinates
        /// </summary>
        /// <param name="userPoint"></param>
        /// <param name="axisOffset"></param>
        /// <param name="xScaleFactor"></param>
        /// <param name="yScaleFactor"></param>
        /// <param name="workingRect"></param>
        /// <returns></returns>
        public static Point ScreenPointFromUserCoords(PointF userPoint, int axisOffset,
                                                  double xScaleFactor, double yScaleFactor,
                                                  double minX, double minY,
                                                  Rectangle workingRect)
        {
            return new Point((int)((userPoint.X - minX)/xScaleFactor) + axisOffset,
                             (int)(workingRect.Height - (userPoint.Y - minY)/yScaleFactor));
        }
    }
}
