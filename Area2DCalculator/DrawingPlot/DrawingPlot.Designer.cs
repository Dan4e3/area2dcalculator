﻿namespace Area2DCalculator.DrawingPlot
{
    partial class DrawingPlot
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DrawingPlot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Name = "DrawingPlot";
            this.Size = new System.Drawing.Size(354, 349);
            this.Load += new System.EventHandler(this.DrawingPlot_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DrawingPlot_Paint);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DrawingPlot_MouseClick);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DrawingPlot_MouseMove);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
