﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

using static Area2DCalculator.DrawingPlot.Utilities;

namespace Area2DCalculator.DrawingPlot
{
    public partial class DrawingPlot : UserControl
    {
        private double MaxX { get; set; } = 10;
        private double MinX { get; set; } = 0;
        private double MaxY { get; set; } = 10;
        private double MinY { get; set; } = 0;
        private int AxesOffset { get; set; } = 40;
        private int AxesArrowsOrtoOffset { get; set; } = 4;
        private int AxesArrowsAlongOffset { get; set; } = 10;
        private Rectangle WorkingRectangle { get; set; }
        private int TicksPerXAxis { get; set; } = 5;
        private int TicksPerYAxis { get; set; } = 5;
        private double XScalingFactor { get; set; } = 0;
        private double YScalingFactior { get; set; } = 0;
        private int TickSize { get; set; } = 3;
        private int PointSize { get; set; } = 8;
        private Pen DefaultPen { get; set; } = Pens.Black;
        private Brush DefaultBrush { get; set; } = new SolidBrush(Color.FromArgb(255, 0, 0, 0));
        private List<DataPoint> PlotPointsList = new List<DataPoint>();
        public DataTable PlotPointsTable { get; } = PlotDataTable.GetDataTable();
        public double PolygonArea { get; private set; } = 0;
        public PointF HoveredUserCoordPoint { get; private set; } = new PointF(0, 0);

        public delegate void SimpleUpdate();
        public event SimpleUpdate OnAreaUpdate;
        public event SimpleUpdate OnMouseOvered;

        public DrawingPlot()
        {
            InitializeComponent();
            DoubleBuffered = true;
        }

        private void DrawingPlot_Paint(object sender, PaintEventArgs e)
        {
            PaintXAxis(e.Graphics, DefaultPen);
            PaintYAxis(e.Graphics, DefaultPen);
            DrawAllData(e.Graphics);
        }

        private void DrawingPlot_MouseClick(object sender, MouseEventArgs e)
        {
            if (PointWithinRectangle(e.Location, WorkingRectangle))
                AddPoint(e.Location, PlotPointsList.Count.ToString());
        }

        private void DrawingPlot_Load(object sender, EventArgs e)
        {
            WorkingRectangle = new Rectangle(AxesOffset, 0, Width - AxesOffset, Height - AxesOffset);
            XScalingFactor = (MaxX - MinX) / WorkingRectangle.Width;
            YScalingFactior = (MaxY - MinY) / WorkingRectangle.Height;
        }

        private void DrawingPlot_MouseMove(object sender, MouseEventArgs e)
        {
            HoveredUserCoordPoint = DataPoint.PointFromScreenCoords(e.Location, AxesOffset, XScalingFactor, YScalingFactior, MinX, MaxY);
            OnMouseOvered();
        }

        public static class PlotDataTable
        {
            public static DataColumn Label = new DataColumn("Label", typeof(string));
            public static DataColumn XVal = new DataColumn("X", typeof(float));
            public static DataColumn YVal = new DataColumn("Y", typeof(float));
            private static DataTable DTable = null;

            public static DataTable GetDataTable()
            {
                if (DTable == null)
                {
                    DTable = new DataTable("PlotData");
                    DTable.Columns.AddRange(new DataColumn[] {
                        XVal, YVal, Label
                    });
                }

                return DTable;
            }

            public static DataRow AddRow(float xVal, float yVal, string label)
            {
                DataRow rw = DTable.NewRow();
                rw[Label] = label;
                rw[XVal] = xVal;
                rw[YVal] = yVal;
                DTable.Rows.Add(rw);

                return rw;
            }
        }


    }
}
