﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using static Area2DCalculator.DrawingPlot.Utilities;

namespace Area2DCalculator.DrawingPlot
{
    public partial class DrawingPlot
    {
        private void DrawPointOnPlot(Graphics g, DataPoint dataPoint)
        {
            Font font = new Font("Arial", 6);
            StringFormat sf = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            g.DrawEllipse(Pens.Black, dataPoint.PointRect);
            g.FillEllipse(DefaultBrush, dataPoint.PointRect);
            g.DrawString(dataPoint.Label, font, DefaultBrush,
                new Point(dataPoint.ScreenPoint.X - PointSize - 3,
                            dataPoint.ScreenPoint.Y + PointSize + 3), sf);

        }

        private void DrawJunctionLines(Graphics g)
        {
            if (PlotPointsList.Count <= 1)
                return;

            g.DrawPolygon(DefaultPen, PlotPointsList.Select(x => x.ScreenPoint).ToArray());

        }

        private void DrawAllData(Graphics g)
        {
            foreach (DataPoint pnt in PlotPointsList)
                DrawPointOnPlot(g, pnt);
            DrawJunctionLines(g);

        }

        private void UpdatePlot()
        {
            if (RescaleRequired())
                RescaleData(MaxX, MaxY, MinX, MinY);
            Invalidate();
            PolygonArea = CalculatePolygonArea(PlotPointsList.Select(x => x.ValuePoint).ToArray());
            OnAreaUpdate();
        }

        private bool RescaleRequired()
        {
            bool rescaleRequired = false;
            foreach (DataPoint pnt in PlotPointsList)
            {
                if (pnt.ValuePoint.X >= MaxX)
                {
                    MaxX = pnt.ValuePoint.X * 1.1;
                    rescaleRequired = true;
                }
                else if (pnt.ValuePoint.X < MinX)
                {
                    MinX = pnt.ValuePoint.X * 1.1;
                    rescaleRequired = true;
                }
                if (pnt.ValuePoint.Y >= MaxY)
                {
                    MaxY = pnt.ValuePoint.Y * 1.1;
                    rescaleRequired = true;
                }
                else if (pnt.ValuePoint.Y < MinY)
                {
                    MinY = pnt.ValuePoint.Y * 1.1;
                    rescaleRequired = true;
                }
            }

            return rescaleRequired;
        }

        private void RescaleData(double maxX, double maxY, double minX, double minY)
        {
            XScalingFactor = (maxX - minX) / WorkingRectangle.Width;
            YScalingFactior = (maxY - minY) / WorkingRectangle.Height;

            foreach (DataPoint pnt in PlotPointsList)
                pnt.Translate(XScalingFactor, YScalingFactior, AxesOffset,
                    WorkingRectangle, PointSize, minX, minY);
        }

        public void RevertAllData()
        {
            MaxX = 10;
            MaxY = 10;
            MinX = 0;
            MinY = 0;
            WorkingRectangle = new Rectangle(AxesOffset, 0, Width - AxesOffset, Height - AxesOffset);
            XScalingFactor = (MaxX - MinX) / WorkingRectangle.Width;
            YScalingFactior = (MaxY - MinY) / WorkingRectangle.Height;
            PolygonArea = 0;
            PlotPointsTable.Clear();
            PlotPointsList.Clear();
            UpdatePlot();
        }

        public void UpdateFromDataGridView(DataTable dtFromView)
        {
            if (dtFromView.Rows.Count == 0)
                return;

            PlotPointsList.Clear();
            foreach (DataRow rw in dtFromView.AsEnumerable())
            {
                if (rw[PlotDataTable.XVal.ColumnName] == DBNull.Value ||
                    rw[PlotDataTable.YVal.ColumnName] == DBNull.Value)
                    continue;

                PointF userDataPoint = new PointF(rw.Field<float>(PlotDataTable.XVal.ColumnName),
                                              rw.Field<float>(PlotDataTable.YVal.ColumnName));
                Point screenPoint = DataPoint.ScreenPointFromUserCoords(userDataPoint, AxesOffset,
                                                                        XScalingFactor, YScalingFactior,
                                                                        MinX, MinY, WorkingRectangle);
                DataPoint dp = new DataPoint(screenPoint, userDataPoint, PointSize, rw[PlotDataTable.Label.ColumnName].ToString());
                PlotPointsList.Add(dp);
            }
            UpdatePlot();
        }

        private void AddPoint(Point screenPoint, string label = "")
        {
            Rectangle pointRect = new Rectangle(screenPoint.X - PointSize / 2, screenPoint.Y - PointSize / 2, PointSize, PointSize);
            PointF valuePoint = DataPoint.PointFromScreenCoords(screenPoint, AxesOffset, XScalingFactor,
                                                  YScalingFactior, MinX, MaxY);
            PlotPointsList.Add(new DataPoint(screenPoint, valuePoint, pointRect, label));
            PlotDataTable.AddRow(valuePoint.X, valuePoint.Y, label);
            UpdatePlot();
        }
    }
}
