﻿using System;
using System.Drawing;

namespace Area2DCalculator.DrawingPlot
{
    public static class Utilities
    {
        /// <summary>
        /// Calculate tick Point positions with provided parameters. Auto adjusts to X and Y axis.
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="endPoint"></param>
        /// <param name="ticksPerAxis"></param>
        /// <returns></returns>
        public static Point[] CalculateTickPositions(Point startPoint, Point endPoint, int ticksPerAxis)
        {
            int pixelIncrement = 0;
            Point[] tickPositions = new Point[ticksPerAxis];

            if (startPoint.Y == endPoint.Y)// if Y'es are equal, then it is X-axis
                pixelIncrement = (endPoint.X - startPoint.X) / ticksPerAxis;
            else
                pixelIncrement = (startPoint.Y - endPoint.Y) / ticksPerAxis;

            for (int i = 0; i < ticksPerAxis; i++)
            {
                Point tickPos;
                if (startPoint.Y == endPoint.Y)
                    tickPos = new Point(startPoint.X + i * pixelIncrement, startPoint.Y);
                else
                    tickPos = new Point(startPoint.X, startPoint.Y - i * pixelIncrement);
                tickPositions[i] = tickPos;
            }

            return tickPositions;
        }

        /// <summary>
        /// Get axis labels array based on minimum and maximum, and ticks per axis values
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="ticksPerAxis"></param>
        /// <returns></returns>
        public static string[] CalculateAxisLabels(double minValue, double maxValue, int ticksPerAxis)
        {
            double perTickIncrement = (maxValue - minValue) / ticksPerAxis;
            string[] labelsArr = new string[ticksPerAxis];
            labelsArr[0] = Math.Round(minValue, 1, MidpointRounding.AwayFromZero).ToString();
            for (int i = 1; i < ticksPerAxis; i++)
            {
                double labelValue = i * perTickIncrement + minValue;
                labelsArr[i] = Math.Round(labelValue, 1, MidpointRounding.AwayFromZero).ToString();
            }

            return labelsArr;
        }

        /// <summary>
        /// Defines whether Point lies inside provided Rectangle area or not
        /// </summary>
        /// <param name="clickedPoint"></param>
        /// <param name="targetRect"></param>
        /// <returns></returns>
        public static bool PointWithinRectangle(Point clickedPoint, Rectangle targetRect)
        {
            if ((clickedPoint.X >= targetRect.X && clickedPoint.X <= targetRect.X + targetRect.Width) &&
                (clickedPoint.Y <= targetRect.Y + targetRect.Height))
                return true;
            return false;
        }

        /// <summary>
        /// Calculate area of polygon using Gauss area formula
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static double CalculatePolygonArea(PointF[] points)
        {
            float sum = 0;
            if (points.Length <= 2)
                return sum;

            for (int i = 0; i < points.Length; i++)
            {
                PointF firstPnt = points[i];
                PointF secondPnt = i == points.Length - 1 ? points[0] : points[i + 1];

                sum += firstPnt.X * secondPnt.Y;
                sum -= firstPnt.Y * secondPnt.X;
            }

            return 0.5 * Math.Abs(sum);
        }
    }
}
