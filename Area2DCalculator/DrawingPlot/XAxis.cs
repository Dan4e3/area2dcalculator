﻿using System.Drawing;
using System.Windows.Forms;

using static Area2DCalculator.DrawingPlot.Utilities;

namespace Area2DCalculator.DrawingPlot
{
    public partial class DrawingPlot : UserControl
    {
        private void PaintXAxis(Graphics g, Pen pen)
        {
            Point bottomLeft = new Point(0, Height - AxesOffset);
            Point bottomRight = new Point(Width, Height - AxesOffset);
            Point zeroCoord = new Point(AxesOffset, Height - AxesOffset);

            g.DrawLine(pen, bottomLeft, bottomRight);

            g.DrawLine(pen, bottomRight, new Point(bottomRight.X - AxesArrowsAlongOffset, bottomRight.Y - AxesArrowsOrtoOffset));
            g.DrawLine(pen, bottomRight, new Point(bottomRight.X - AxesArrowsAlongOffset, bottomRight.Y + AxesArrowsOrtoOffset));

            Point[] tickPositions = CalculateTickPositions(zeroCoord, bottomRight, TicksPerXAxis);
            string[] xLabels = CalculateAxisLabels(MinX, MaxX, TicksPerXAxis);
            PaintXTicks(g, pen, tickPositions);
            PaintXLabels(g, pen, tickPositions, xLabels);
        }
        private void PaintXTicks(Graphics g, Pen pen, Point[] tickPositions)
        {
            foreach (Point pnt in tickPositions)
                g.DrawLine(pen, pnt.X, pnt.Y, pnt.X, pnt.Y + TickSize);
        }

        private void PaintXLabels(Graphics g, Pen pen, Point[] tickPositions, string[] labels)
        {
            Font font = new Font("Arial", 8);
            StringFormat sf = new StringFormat
            {
                Alignment = StringAlignment.Center
            };

            for (int i = 0; i < tickPositions.Length; i++)
                g.DrawString(labels[i], font, Brushes.Black,
                    new Point(tickPositions[i].X,
                              tickPositions[i].Y + TickSize + (int)font.Size / 2),
                    sf);

        }
    }
}
