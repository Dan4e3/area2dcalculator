﻿using System.Drawing;
using System.Windows.Forms;

using static Area2DCalculator.DrawingPlot.Utilities;

namespace Area2DCalculator.DrawingPlot
{
    public partial class DrawingPlot : UserControl
    {
        private void PaintYAxis(Graphics g, Pen pen)
        {
            Point leftTop = new Point(AxesOffset, 0);
            Point leftBottom = new Point(AxesOffset, Height);
            Point zeroCoord = new Point(AxesOffset, Height - AxesOffset);

            g.DrawLine(pen, leftTop, leftBottom);

            g.DrawLine(pen, leftTop, new Point(leftTop.X - AxesArrowsOrtoOffset, leftTop.Y + AxesArrowsAlongOffset));
            g.DrawLine(pen, leftTop, new Point(leftTop.X + AxesArrowsOrtoOffset, leftTop.Y + AxesArrowsAlongOffset));

            Point[] tickPositions = CalculateTickPositions(zeroCoord, leftTop, TicksPerYAxis);
            string[] tickLabels = CalculateAxisLabels(MinY, MaxY, TicksPerYAxis);
            PaintYTicks(g, pen, tickPositions);
            PaintYLabels(g, pen, tickPositions, tickLabels);
        }

        private void PaintYTicks(Graphics g, Pen pen, Point[] tickPositions)
        {
            foreach (Point pnt in tickPositions)
                g.DrawLine(pen, pnt.X, pnt.Y, pnt.X - TickSize, pnt.Y);
        }

        private void PaintYLabels(Graphics g, Pen pen, Point[] tickPositions, string[] labels)
        {
            Font font = new Font("Arial", 8);
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;

            for (int i = 0; i < tickPositions.Length; i++)
                g.DrawString(labels[i], font, Brushes.Black,
                    new Point(tickPositions[i].X - TickSize - (int)font.Size,
                              tickPositions[i].Y),
                    sf);
        }
    }
}
