﻿namespace Area2DCalculator
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.PointsDatagridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.PolygonAreaLabel = new System.Windows.Forms.Label();
            this.CurrentXCoordLabel = new System.Windows.Forms.Label();
            this.LoadDataBtn = new System.Windows.Forms.Button();
            this.CurrentYCoordLabel = new System.Windows.Forms.Label();
            this.RevertPlotBtn = new System.Windows.Forms.Button();
            this.HelpBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Plot = new Area2DCalculator.DrawingPlot.DrawingPlot();
            ((System.ComponentModel.ISupportInitialize)(this.PointsDatagridView)).BeginInit();
            this.SuspendLayout();
            // 
            // PointsDatagridView
            // 
            this.PointsDatagridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PointsDatagridView.Location = new System.Drawing.Point(12, 32);
            this.PointsDatagridView.Name = "PointsDatagridView";
            this.PointsDatagridView.Size = new System.Drawing.Size(362, 422);
            this.PointsDatagridView.TabIndex = 1;
            this.PointsDatagridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.PointsDatagridView_CellValueChanged);
            this.PointsDatagridView.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.PointsDatagridView_RowsRemoved);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(747, 460);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Area of polygon:";
            // 
            // PolygonAreaLabel
            // 
            this.PolygonAreaLabel.AutoSize = true;
            this.PolygonAreaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PolygonAreaLabel.Location = new System.Drawing.Point(877, 460);
            this.PolygonAreaLabel.Name = "PolygonAreaLabel";
            this.PolygonAreaLabel.Size = new System.Drawing.Size(0, 20);
            this.PolygonAreaLabel.TabIndex = 3;
            // 
            // CurrentXCoordLabel
            // 
            this.CurrentXCoordLabel.AutoSize = true;
            this.CurrentXCoordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CurrentXCoordLabel.Location = new System.Drawing.Point(391, 460);
            this.CurrentXCoordLabel.Name = "CurrentXCoordLabel";
            this.CurrentXCoordLabel.Size = new System.Drawing.Size(0, 20);
            this.CurrentXCoordLabel.TabIndex = 5;
            // 
            // LoadDataBtn
            // 
            this.LoadDataBtn.Location = new System.Drawing.Point(117, 460);
            this.LoadDataBtn.Name = "LoadDataBtn";
            this.LoadDataBtn.Size = new System.Drawing.Size(135, 41);
            this.LoadDataBtn.TabIndex = 6;
            this.LoadDataBtn.Text = "Load data from text file";
            this.LoadDataBtn.UseVisualStyleBackColor = true;
            this.LoadDataBtn.Click += new System.EventHandler(this.LoadDataBtn_Click);
            // 
            // CurrentYCoordLabel
            // 
            this.CurrentYCoordLabel.AutoSize = true;
            this.CurrentYCoordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CurrentYCoordLabel.Location = new System.Drawing.Point(464, 460);
            this.CurrentYCoordLabel.Name = "CurrentYCoordLabel";
            this.CurrentYCoordLabel.Size = new System.Drawing.Size(0, 20);
            this.CurrentYCoordLabel.TabIndex = 7;
            // 
            // RevertPlotBtn
            // 
            this.RevertPlotBtn.Location = new System.Drawing.Point(903, 5);
            this.RevertPlotBtn.Name = "RevertPlotBtn";
            this.RevertPlotBtn.Size = new System.Drawing.Size(90, 24);
            this.RevertPlotBtn.TabIndex = 8;
            this.RevertPlotBtn.Text = "Revert plot (X)";
            this.RevertPlotBtn.UseVisualStyleBackColor = true;
            this.RevertPlotBtn.Click += new System.EventHandler(this.RevertPlotBtn_Click);
            // 
            // HelpBtn
            // 
            this.HelpBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HelpBtn.Location = new System.Drawing.Point(258, 460);
            this.HelpBtn.Name = "HelpBtn";
            this.HelpBtn.Size = new System.Drawing.Size(20, 24);
            this.HelpBtn.TabIndex = 9;
            this.HelpBtn.Text = "?";
            this.HelpBtn.UseVisualStyleBackColor = true;
            this.HelpBtn.Click += new System.EventHandler(this.HelpBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(576, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(262, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Left click on plot to add some points";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(95, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Add and edit data here";
            // 
            // Plot
            // 
            this.Plot.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Plot.Location = new System.Drawing.Point(391, 32);
            this.Plot.Name = "Plot";
            this.Plot.Size = new System.Drawing.Size(602, 422);
            this.Plot.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 512);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.HelpBtn);
            this.Controls.Add(this.RevertPlotBtn);
            this.Controls.Add(this.CurrentYCoordLabel);
            this.Controls.Add(this.LoadDataBtn);
            this.Controls.Add(this.CurrentXCoordLabel);
            this.Controls.Add(this.PolygonAreaLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PointsDatagridView);
            this.Controls.Add(this.Plot);
            this.Name = "MainForm";
            this.Text = "Area2DCalculator";
            ((System.ComponentModel.ISupportInitialize)(this.PointsDatagridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DrawingPlot.DrawingPlot Plot;
        private System.Windows.Forms.DataGridView PointsDatagridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label PolygonAreaLabel;
        private System.Windows.Forms.Label CurrentXCoordLabel;
        private System.Windows.Forms.Button LoadDataBtn;
        private System.Windows.Forms.Label CurrentYCoordLabel;
        private System.Windows.Forms.Button RevertPlotBtn;
        private System.Windows.Forms.Button HelpBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

