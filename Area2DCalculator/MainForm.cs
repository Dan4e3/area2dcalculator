﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DP = Area2DCalculator.DrawingPlot;

namespace Area2DCalculator
{
    public partial class MainForm : Form
    {
        List<DataGridViewRow> PendingDataGridViewRows = new List<DataGridViewRow>();

        public MainForm()
        {
            InitializeComponent();
            PointsDatagridView.AutoGenerateColumns = true;
            PointsDatagridView.DataSource = Plot.PlotPointsTable;

            Plot.OnAreaUpdate += DrawingPlot_OnAreaUpdate;
            Plot.OnMouseOvered += Plot_OnMouseOvered;
        }

        private void Plot_OnMouseOvered()
        {
            CurrentXCoordLabel.Text = $"X: {Math.Round(Plot.HoveredUserCoordPoint.X, 2, MidpointRounding.AwayFromZero)}";
            CurrentYCoordLabel.Text = $"Y: {Math.Round(Plot.HoveredUserCoordPoint.Y, 2, MidpointRounding.AwayFromZero)}";
        }

        private void DrawingPlot_OnAreaUpdate()
        {
            PolygonAreaLabel.Text = Math.Round(Plot.PolygonArea, 2).ToString();
        }

        private void PointsDatagridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataTable dtFromView = (PointsDatagridView.DataSource as DataTable).DefaultView.ToTable();
            Plot.UpdateFromDataGridView(dtFromView);
        }

        private void PointsDatagridView_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            DataTable dtFromView = (PointsDatagridView.DataSource as DataTable).DefaultView.ToTable();
            Plot.UpdateFromDataGridView(dtFromView);
        }

        private void LoadDataBtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog fd = new OpenFileDialog())
            {
                fd.Multiselect = false;
                fd.Filter = "Text files (*.txt)|*.txt";
                DialogResult res = fd.ShowDialog();
                if (res == DialogResult.OK)
                    ThreadPool.QueueUserWorkItem(_ => LoadDataFromFile(fd.FileName, Plot.PlotPointsTable, PointsDatagridView));
            }
        }

        private void LoadDataFromFile(string filePath, DataTable dt, DataGridView dgv)
        {
            string[] fileContents = File.ReadAllLines(filePath);
            foreach (string line in fileContents)
            {
                string trimmedLine = line.Replace(" ", "");
                string[] splitted;
                if (trimmedLine.Contains(';'))
                    splitted = trimmedLine.Split(';');
                else
                    continue;

                if (splitted.Length >= 2)
                {
                    DataRow rw = dt.NewRow();
                    rw.ItemArray = splitted;
                    dt.Rows.Add(rw);
                }
            }
            
            Plot.Invoke(new Action(() => Plot.UpdateFromDataGridView(dt)));
            dgv.Invoke(new Action(() => dgv.Refresh()));
            dgv.Invoke(new Action(() => dgv.AutoResizeColumns()));
        }

        private void RevertPlotBtn_Click(object sender, EventArgs e)
        {
            Plot.RevertAllData();
        }

        private void HelpBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Each point data must be located on new line. Individual " +
                "values must be separated by ';' symbol. Floating point numbers should be " +
                "used with system defined floating point symbol ('.' or ','). First value " +
                "stands for X coordinate, second for Y, third one is string label for this point. " +
                "Example of single point input: '2;3;1'.");
        }
    }
}
