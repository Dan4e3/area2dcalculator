﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

using Area2DCalculator.DrawingPlot;
using static Area2DCalculator.DrawingPlot.Utilities;

namespace Area2DCalculatorTests
{
    [TestClass]
    public class DrawingPlotTests
    {
        readonly DrawingPlot drawingPlot = new DrawingPlot();

        [TestMethod]
        public void CalculateTickPositions_ReturnsPositionsArray()
        {
            Point[] result = CalculateTickPositions(new Point(0, 10), new Point(100, 10), 5);
            Point[] expected = new Point[5] {
                new Point(0, 10),
                new Point(20, 10),
                new Point(40, 10),
                new Point(60, 10),
                new Point(80, 10),
            };
            CollectionAssert.AreEqual(expected, result);

            result = CalculateTickPositions(new Point(0, 10), new Point(94, 10), 5);
            expected = new Point[5] {
                new Point(0, 10),
                new Point(18, 10),
                new Point(36, 10),
                new Point(54, 10),
                new Point(72, 10),
            };
            CollectionAssert.AreEqual(expected, result);

            result = null;
            result = CalculateTickPositions(new Point(0, 10), new Point(90, 10), 7);
            expected = new Point[7] {
                new Point(0, 10),
                new Point(12, 10),
                new Point(24, 10),
                new Point(36, 10),
                new Point(48, 10),
                new Point(60, 10),
                new Point(72, 10),
            };
            CollectionAssert.AreEqual(expected, result);

            result = null;
            result = CalculateTickPositions(new Point(0, 100), new Point(0, 20), 4);
            expected = new Point[4] {
                new Point(0, 100),
                new Point(0, 80),
                new Point(0, 60),
                new Point(0, 40),
            };
        }

        [TestMethod]
        public void CalculateAxisLabels_ReturnsAxisLabelsArray()
        {
            string[] result = CalculateAxisLabels(0, 20, 5);
            string[] expected = new string[5] {
                "0", "4", "8", "12", "16"
            };
            CollectionAssert.AreEqual(expected, result);

            result = null;
            result = CalculateAxisLabels(2, 20, 6);
            expected = new string[6] {
                "2", "5", "8", "11", "14", "17"
            };
            CollectionAssert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PointWithinRectangle_ReturnsTrueIfWithinRect()
        {
            bool result = PointWithinRectangle(new Point(5, 5), new Rectangle(0, 0, 20, 20));
            bool expected = true;
            Assert.AreEqual(expected, result);

            result = PointWithinRectangle(new Point(25, 5), new Rectangle(0, 0, 20, 20));
            expected = false;
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void CalculatePolygonArea_ReturnsAreaIfEligibleInput()
        {
            double result = CalculatePolygonArea(new PointF[] {
                new PointF(1,1),
                new PointF(2,2),
                new PointF(3,1)
            });
            double expected = 0.5 * Math.Abs(7 - 9);
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void PointFromScreenCoords_ReturnsPointInUserCoords()
        {
            PointF result = DataPoint.PointFromScreenCoords(new Point(10, 10), 5, 2, 2, 0, 10);
            PointF expected = new PointF(10, -10);
            Assert.AreEqual(expected, result);
        }
    }
}
